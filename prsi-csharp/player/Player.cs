﻿using System.Collections.Immutable;
using src.card;

namespace src.player;

public class Player
{
    private readonly IList<Card> _deck;
    private readonly string _name;
    private int _deckSize;

    public Player(string name)
    {
        _deck = new List<Card>();
        _deckSize = 0;
        _name = name;
    }

    public void ClearDeck()
    {
        _deck.Clear();
    }

    public void AddCardToDeck(Card card)
    {
        _deck.Add(card);
        _deckSize += 1;
    }

    public void AddCardsToDeck(IEnumerable<Card> cards)
    {
        foreach (var t in cards) AddCardToDeck(t);
    }
    
    public void RemoveCardFromDeck(Card card) {
        _deck.Remove(card);
        _deckSize--;
    }
    
    public string Name => _name;
    public bool IsPlaying => _deckSize > 0;
    public IList<Card> Deck => _deck.ToImmutableList();

    public override string ToString()
    {
        return $"Player(name: {Name})";
    }
}