﻿using System.Collections.Immutable;
using Newtonsoft.Json;
using src.card;

namespace src.translations;

public class CzechTranslation : Translation
{
    const string _stringTranslationsPath = "translations/czech.json";
    const string _internationalLanguageName = "Czech";
    const string _languageName = "Čeština";

    public CzechTranslation() : base(_internationalLanguageName, _languageName) {}

    private static string ToValue(Card card)
    {
        return $"{GetNameForColorByCard(card)} {GetNameForType(card.Type)}";
    }
    
    protected override IDictionary<Card, string> CardTranslation()
    {
        return Card.Cards
            .AllCards
            .ToImmutableDictionary(card => card, ToValue);
    }

    protected override IDictionary<CardColor, string> CardColorTranslation()
    {
        return Enum
            .GetValues<CardColor>()
            .ToImmutableDictionary(cc => cc, GetNameForColorByCardColor);
    }

    protected override IDictionary<string, string> StringTranslation()
    {
        return JsonConvert.DeserializeObject<IDictionary<string, string>>(File.ReadAllText(_stringTranslationsPath))??new Dictionary<string, string>();
    }
    
    private static string GetNameForType(CardType type)
    {
        return type switch
        {
            CardType.Seven => "sedma",
            CardType.Eight => "osma",
            CardType.Nine => "devítka",
            CardType.X => "desítka",
            CardType.Eso => "eso",
            CardType.Kral => "král",
            CardType.Menic => "měnič",
            CardType.Svrsek => "svršek",
            _ => throw new ArgumentOutOfRangeException(nameof(type), type, null)
        };
    }

    private static string GetNameForColorByCard(Card card)
    {
        if(card.Type is CardType.Eso or CardType.Menic or CardType.Svrsek or CardType.Kral)
        {
            return card.Color switch 
            {
                CardColor.Kule => "kuloví",
                CardColor.Listy => "zelený",
                CardColor.Cerveny => "červený",
                CardColor.Zaludy => "žaludoví",
                _ => throw new ArgumentOutOfRangeException()
            };
        }

        return card.Color switch
        {
            CardColor.Kule => "kulová",
            CardColor.Listy => "zelená",
            CardColor.Cerveny => "červená",
            CardColor.Zaludy => "žaludobá",
            _ => ""
        };
    }

    private static string GetNameForColorByCardColor(CardColor color)
    {
        return color switch
        {
            CardColor.Kule => "kule",
            CardColor.Zaludy => "žaludy",
            CardColor.Listy => "listy",
            CardColor.Cerveny => "červený",
            _ => ""
        };
    }
}