﻿using src.card;
using src.player;

namespace src.intarface;

public interface IGameInterface
{
    void GameStart();
    Card? LetPlayerSelectCard(Player player, Card currentCard, CardColor? colorOnChanger, int? overChargeCount);
    void ShowFirstCard(Card currentCard);
    void PlaceCard(Card selectedCard);
    void AddCardToDeck(Player player, Card retrievedCard);
    CardColor? LetPlayerSelectColor();
    void PlayerRetrievedOverChargedCards(Player player, int count);
    void GameOver(Players players, int round);
    void CurrentPlayer(Player player);
}